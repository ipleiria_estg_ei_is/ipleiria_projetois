﻿namespace AlarmingSystem
{
    partial class AlarmSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxLessH = new System.Windows.Forms.CheckBox();
            this.checkBoxLessP = new System.Windows.Forms.CheckBox();
            this.checkBoxLessT = new System.Windows.Forms.CheckBox();
            this.checkBoxGreaterH = new System.Windows.Forms.CheckBox();
            this.checkBoxGreaterP = new System.Windows.Forms.CheckBox();
            this.checkBoxGreaterT = new System.Windows.Forms.CheckBox();
            this.numericUpDownHumidadeGreater = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownPressaoGreater = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTempGreater = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHumidadeLess = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownPressaoLess = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTempLess = new System.Windows.Forms.NumericUpDown();
            this.labelGreaterHum = new System.Windows.Forms.Label();
            this.labelGreaterPress = new System.Windows.Forms.Label();
            this.labelGreaterTemp = new System.Windows.Forms.Label();
            this.labelLessHum = new System.Windows.Forms.Label();
            this.labelLessPress = new System.Windows.Forms.Label();
            this.labelLessTemp = new System.Windows.Forms.Label();
            this.labelHum = new System.Windows.Forms.Label();
            this.labelPress = new System.Windows.Forms.Label();
            this.labelTemp = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.checkBoxEqualsH = new System.Windows.Forms.CheckBox();
            this.checkBoxEqualsP = new System.Windows.Forms.CheckBox();
            this.checkBoxEqualsT = new System.Windows.Forms.CheckBox();
            this.numericUpDownHumidadeEquals = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownPressaoEquals = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTempEquals = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHumidadeGreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPressaoGreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTempGreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHumidadeLess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPressaoLess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTempLess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHumidadeEquals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPressaoEquals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTempEquals)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(12, 260);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(727, 81);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxEqualsH);
            this.groupBox1.Controls.Add(this.checkBoxEqualsP);
            this.groupBox1.Controls.Add(this.checkBoxEqualsT);
            this.groupBox1.Controls.Add(this.numericUpDownHumidadeEquals);
            this.groupBox1.Controls.Add(this.numericUpDownPressaoEquals);
            this.groupBox1.Controls.Add(this.numericUpDownTempEquals);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.checkBoxLessH);
            this.groupBox1.Controls.Add(this.checkBoxLessP);
            this.groupBox1.Controls.Add(this.checkBoxLessT);
            this.groupBox1.Controls.Add(this.checkBoxGreaterH);
            this.groupBox1.Controls.Add(this.checkBoxGreaterP);
            this.groupBox1.Controls.Add(this.checkBoxGreaterT);
            this.groupBox1.Controls.Add(this.numericUpDownHumidadeGreater);
            this.groupBox1.Controls.Add(this.numericUpDownPressaoGreater);
            this.groupBox1.Controls.Add(this.numericUpDownTempGreater);
            this.groupBox1.Controls.Add(this.numericUpDownHumidadeLess);
            this.groupBox1.Controls.Add(this.numericUpDownPressaoLess);
            this.groupBox1.Controls.Add(this.numericUpDownTempLess);
            this.groupBox1.Controls.Add(this.labelGreaterHum);
            this.groupBox1.Controls.Add(this.labelGreaterPress);
            this.groupBox1.Controls.Add(this.labelGreaterTemp);
            this.groupBox1.Controls.Add(this.labelLessHum);
            this.groupBox1.Controls.Add(this.labelLessPress);
            this.groupBox1.Controls.Add(this.labelLessTemp);
            this.groupBox1.Controls.Add(this.labelHum);
            this.groupBox1.Controls.Add(this.labelPress);
            this.groupBox1.Controls.Add(this.labelTemp);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(727, 242);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Alarm Settings";
            // 
            // checkBoxLessH
            // 
            this.checkBoxLessH.AutoSize = true;
            this.checkBoxLessH.Location = new System.Drawing.Point(185, 204);
            this.checkBoxLessH.Name = "checkBoxLessH";
            this.checkBoxLessH.Size = new System.Drawing.Size(59, 17);
            this.checkBoxLessH.TabIndex = 29;
            this.checkBoxLessH.Text = "Enable";
            this.checkBoxLessH.UseVisualStyleBackColor = true;
            this.checkBoxLessH.CheckedChanged += new System.EventHandler(this.checkBoxOptionHumidadeLess_CheckedChanged);
            // 
            // checkBoxLessP
            // 
            this.checkBoxLessP.AutoSize = true;
            this.checkBoxLessP.Location = new System.Drawing.Point(185, 134);
            this.checkBoxLessP.Name = "checkBoxLessP";
            this.checkBoxLessP.Size = new System.Drawing.Size(59, 17);
            this.checkBoxLessP.TabIndex = 28;
            this.checkBoxLessP.Text = "Enable";
            this.checkBoxLessP.UseVisualStyleBackColor = true;
            this.checkBoxLessP.CheckedChanged += new System.EventHandler(this.checkBoxOptionPressaoLess_CheckedChanged);
            // 
            // checkBoxLessT
            // 
            this.checkBoxLessT.AutoSize = true;
            this.checkBoxLessT.Location = new System.Drawing.Point(185, 57);
            this.checkBoxLessT.Name = "checkBoxLessT";
            this.checkBoxLessT.Size = new System.Drawing.Size(59, 17);
            this.checkBoxLessT.TabIndex = 27;
            this.checkBoxLessT.Text = "Enable";
            this.checkBoxLessT.UseVisualStyleBackColor = true;
            this.checkBoxLessT.CheckedChanged += new System.EventHandler(this.checkBoxOptionTemperaturaLess_CheckedChanged);
            // 
            // checkBoxGreaterH
            // 
            this.checkBoxGreaterH.AutoSize = true;
            this.checkBoxGreaterH.Location = new System.Drawing.Point(430, 204);
            this.checkBoxGreaterH.Name = "checkBoxGreaterH";
            this.checkBoxGreaterH.Size = new System.Drawing.Size(59, 17);
            this.checkBoxGreaterH.TabIndex = 26;
            this.checkBoxGreaterH.Text = "Enable";
            this.checkBoxGreaterH.UseVisualStyleBackColor = true;
            this.checkBoxGreaterH.CheckedChanged += new System.EventHandler(this.checkBoxOptionHumidadeGreater_CheckedChanged);
            // 
            // checkBoxGreaterP
            // 
            this.checkBoxGreaterP.AutoSize = true;
            this.checkBoxGreaterP.Location = new System.Drawing.Point(430, 134);
            this.checkBoxGreaterP.Name = "checkBoxGreaterP";
            this.checkBoxGreaterP.Size = new System.Drawing.Size(59, 17);
            this.checkBoxGreaterP.TabIndex = 25;
            this.checkBoxGreaterP.Text = "Enable";
            this.checkBoxGreaterP.UseVisualStyleBackColor = true;
            this.checkBoxGreaterP.CheckedChanged += new System.EventHandler(this.checkBoxOptionPressaoGreater_CheckedChanged);
            // 
            // checkBoxGreaterT
            // 
            this.checkBoxGreaterT.AutoSize = true;
            this.checkBoxGreaterT.Location = new System.Drawing.Point(430, 57);
            this.checkBoxGreaterT.Name = "checkBoxGreaterT";
            this.checkBoxGreaterT.Size = new System.Drawing.Size(59, 17);
            this.checkBoxGreaterT.TabIndex = 24;
            this.checkBoxGreaterT.Text = "Enable";
            this.checkBoxGreaterT.UseVisualStyleBackColor = true;
            this.checkBoxGreaterT.CheckedChanged += new System.EventHandler(this.checkBoxOptionTemperaturaGreater_CheckedChanged);
            // 
            // numericUpDownHumidadeGreater
            // 
            this.numericUpDownHumidadeGreater.DecimalPlaces = 1;
            this.numericUpDownHumidadeGreater.Enabled = false;
            this.numericUpDownHumidadeGreater.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownHumidadeGreater.Location = new System.Drawing.Point(356, 201);
            this.numericUpDownHumidadeGreater.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownHumidadeGreater.Name = "numericUpDownHumidadeGreater";
            this.numericUpDownHumidadeGreater.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownHumidadeGreater.TabIndex = 22;
            // 
            // numericUpDownPressaoGreater
            // 
            this.numericUpDownPressaoGreater.DecimalPlaces = 1;
            this.numericUpDownPressaoGreater.Enabled = false;
            this.numericUpDownPressaoGreater.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownPressaoGreater.Location = new System.Drawing.Point(356, 131);
            this.numericUpDownPressaoGreater.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownPressaoGreater.Name = "numericUpDownPressaoGreater";
            this.numericUpDownPressaoGreater.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownPressaoGreater.TabIndex = 21;
            // 
            // numericUpDownTempGreater
            // 
            this.numericUpDownTempGreater.DecimalPlaces = 1;
            this.numericUpDownTempGreater.Enabled = false;
            this.numericUpDownTempGreater.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownTempGreater.Location = new System.Drawing.Point(356, 56);
            this.numericUpDownTempGreater.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownTempGreater.Name = "numericUpDownTempGreater";
            this.numericUpDownTempGreater.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownTempGreater.TabIndex = 20;
            // 
            // numericUpDownHumidadeLess
            // 
            this.numericUpDownHumidadeLess.DecimalPlaces = 1;
            this.numericUpDownHumidadeLess.Enabled = false;
            this.numericUpDownHumidadeLess.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownHumidadeLess.Location = new System.Drawing.Point(110, 201);
            this.numericUpDownHumidadeLess.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownHumidadeLess.Name = "numericUpDownHumidadeLess";
            this.numericUpDownHumidadeLess.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownHumidadeLess.TabIndex = 19;
            // 
            // numericUpDownPressaoLess
            // 
            this.numericUpDownPressaoLess.DecimalPlaces = 1;
            this.numericUpDownPressaoLess.Enabled = false;
            this.numericUpDownPressaoLess.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownPressaoLess.Location = new System.Drawing.Point(110, 128);
            this.numericUpDownPressaoLess.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownPressaoLess.Name = "numericUpDownPressaoLess";
            this.numericUpDownPressaoLess.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownPressaoLess.TabIndex = 18;
            // 
            // numericUpDownTempLess
            // 
            this.numericUpDownTempLess.DecimalPlaces = 1;
            this.numericUpDownTempLess.Enabled = false;
            this.numericUpDownTempLess.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownTempLess.Location = new System.Drawing.Point(110, 56);
            this.numericUpDownTempLess.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownTempLess.Name = "numericUpDownTempLess";
            this.numericUpDownTempLess.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownTempLess.TabIndex = 17;
            // 
            // labelGreaterHum
            // 
            this.labelGreaterHum.AutoSize = true;
            this.labelGreaterHum.Location = new System.Drawing.Point(277, 203);
            this.labelGreaterHum.Name = "labelGreaterHum";
            this.labelGreaterHum.Size = new System.Drawing.Size(73, 13);
            this.labelGreaterHum.TabIndex = 16;
            this.labelGreaterHum.Text = "Greater Than:";
            // 
            // labelGreaterPress
            // 
            this.labelGreaterPress.AutoSize = true;
            this.labelGreaterPress.Location = new System.Drawing.Point(277, 134);
            this.labelGreaterPress.Name = "labelGreaterPress";
            this.labelGreaterPress.Size = new System.Drawing.Size(73, 13);
            this.labelGreaterPress.TabIndex = 15;
            this.labelGreaterPress.Text = "Greater Than:";
            // 
            // labelGreaterTemp
            // 
            this.labelGreaterTemp.AutoSize = true;
            this.labelGreaterTemp.Location = new System.Drawing.Point(277, 58);
            this.labelGreaterTemp.Name = "labelGreaterTemp";
            this.labelGreaterTemp.Size = new System.Drawing.Size(73, 13);
            this.labelGreaterTemp.TabIndex = 14;
            this.labelGreaterTemp.Text = "Greater Than:";
            // 
            // labelLessHum
            // 
            this.labelLessHum.AutoSize = true;
            this.labelLessHum.Location = new System.Drawing.Point(44, 203);
            this.labelLessHum.Name = "labelLessHum";
            this.labelLessHum.Size = new System.Drawing.Size(60, 13);
            this.labelLessHum.TabIndex = 13;
            this.labelLessHum.Text = "Less Than:";
            // 
            // labelLessPress
            // 
            this.labelLessPress.AutoSize = true;
            this.labelLessPress.Location = new System.Drawing.Point(45, 134);
            this.labelLessPress.Name = "labelLessPress";
            this.labelLessPress.Size = new System.Drawing.Size(60, 13);
            this.labelLessPress.TabIndex = 12;
            this.labelLessPress.Text = "Less Than:";
            // 
            // labelLessTemp
            // 
            this.labelLessTemp.AutoSize = true;
            this.labelLessTemp.Location = new System.Drawing.Point(44, 58);
            this.labelLessTemp.Name = "labelLessTemp";
            this.labelLessTemp.Size = new System.Drawing.Size(60, 13);
            this.labelLessTemp.TabIndex = 11;
            this.labelLessTemp.Text = "Less Than:";
            // 
            // labelHum
            // 
            this.labelHum.AutoSize = true;
            this.labelHum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHum.Location = new System.Drawing.Point(21, 179);
            this.labelHum.Name = "labelHum";
            this.labelHum.Size = new System.Drawing.Size(59, 13);
            this.labelHum.TabIndex = 4;
            this.labelHum.Text = "Humidity:";
            // 
            // labelPress
            // 
            this.labelPress.AutoSize = true;
            this.labelPress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPress.Location = new System.Drawing.Point(21, 108);
            this.labelPress.Name = "labelPress";
            this.labelPress.Size = new System.Drawing.Size(124, 13);
            this.labelPress.TabIndex = 3;
            this.labelPress.Text = "Barometric Pressure:";
            // 
            // labelTemp
            // 
            this.labelTemp.AutoSize = true;
            this.labelTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTemp.Location = new System.Drawing.Point(21, 36);
            this.labelTemp.Name = "labelTemp";
            this.labelTemp.Size = new System.Drawing.Size(82, 13);
            this.labelTemp.TabIndex = 2;
            this.labelTemp.Text = "Temperature:";
            // 
            // checkBoxEqualsH
            // 
            this.checkBoxEqualsH.AutoSize = true;
            this.checkBoxEqualsH.Location = new System.Drawing.Point(649, 204);
            this.checkBoxEqualsH.Name = "checkBoxEqualsH";
            this.checkBoxEqualsH.Size = new System.Drawing.Size(59, 17);
            this.checkBoxEqualsH.TabIndex = 38;
            this.checkBoxEqualsH.Text = "Enable";
            this.checkBoxEqualsH.UseVisualStyleBackColor = true;
            this.checkBoxEqualsH.CheckedChanged += new System.EventHandler(this.checkBoxEqualsH_CheckedChanged);
            // 
            // checkBoxEqualsP
            // 
            this.checkBoxEqualsP.AutoSize = true;
            this.checkBoxEqualsP.Location = new System.Drawing.Point(649, 134);
            this.checkBoxEqualsP.Name = "checkBoxEqualsP";
            this.checkBoxEqualsP.Size = new System.Drawing.Size(59, 17);
            this.checkBoxEqualsP.TabIndex = 37;
            this.checkBoxEqualsP.Text = "Enable";
            this.checkBoxEqualsP.UseVisualStyleBackColor = true;
            this.checkBoxEqualsP.CheckedChanged += new System.EventHandler(this.checkBoxEqualsP_CheckedChanged);
            // 
            // checkBoxEqualsT
            // 
            this.checkBoxEqualsT.AutoSize = true;
            this.checkBoxEqualsT.Location = new System.Drawing.Point(649, 57);
            this.checkBoxEqualsT.Name = "checkBoxEqualsT";
            this.checkBoxEqualsT.Size = new System.Drawing.Size(59, 17);
            this.checkBoxEqualsT.TabIndex = 36;
            this.checkBoxEqualsT.Text = "Enable";
            this.checkBoxEqualsT.UseVisualStyleBackColor = true;
            this.checkBoxEqualsT.CheckedChanged += new System.EventHandler(this.checkBoxEqualsT_CheckedChanged);
            // 
            // numericUpDownHumidadeEquals
            // 
            this.numericUpDownHumidadeEquals.DecimalPlaces = 1;
            this.numericUpDownHumidadeEquals.Enabled = false;
            this.numericUpDownHumidadeEquals.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownHumidadeEquals.Location = new System.Drawing.Point(575, 201);
            this.numericUpDownHumidadeEquals.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownHumidadeEquals.Name = "numericUpDownHumidadeEquals";
            this.numericUpDownHumidadeEquals.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownHumidadeEquals.TabIndex = 35;
            // 
            // numericUpDownPressaoEquals
            // 
            this.numericUpDownPressaoEquals.DecimalPlaces = 1;
            this.numericUpDownPressaoEquals.Enabled = false;
            this.numericUpDownPressaoEquals.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownPressaoEquals.Location = new System.Drawing.Point(575, 131);
            this.numericUpDownPressaoEquals.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownPressaoEquals.Name = "numericUpDownPressaoEquals";
            this.numericUpDownPressaoEquals.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownPressaoEquals.TabIndex = 34;
            // 
            // numericUpDownTempEquals
            // 
            this.numericUpDownTempEquals.DecimalPlaces = 1;
            this.numericUpDownTempEquals.Enabled = false;
            this.numericUpDownTempEquals.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownTempEquals.Location = new System.Drawing.Point(575, 56);
            this.numericUpDownTempEquals.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownTempEquals.Name = "numericUpDownTempEquals";
            this.numericUpDownTempEquals.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownTempEquals.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(527, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Equals:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(527, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Equals:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(527, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Equals:";
            // 
            // AlarmSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 359);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSave);
            this.MaximizeBox = false;
            this.Name = "AlarmSettingsForm";
            this.Text = "Alarm Settings ";
            this.Load += new System.EventHandler(this.AlarmSettingsForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHumidadeGreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPressaoGreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTempGreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHumidadeLess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPressaoLess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTempLess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHumidadeEquals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPressaoEquals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTempEquals)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelGreaterHum;
        private System.Windows.Forms.Label labelGreaterPress;
        private System.Windows.Forms.Label labelGreaterTemp;
        private System.Windows.Forms.Label labelLessHum;
        private System.Windows.Forms.Label labelLessPress;
        private System.Windows.Forms.Label labelLessTemp;
        private System.Windows.Forms.Label labelHum;
        private System.Windows.Forms.Label labelPress;
        private System.Windows.Forms.Label labelTemp;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.NumericUpDown numericUpDownHumidadeGreater;
        private System.Windows.Forms.NumericUpDown numericUpDownPressaoGreater;
        private System.Windows.Forms.NumericUpDown numericUpDownTempGreater;
        private System.Windows.Forms.NumericUpDown numericUpDownHumidadeLess;
        private System.Windows.Forms.NumericUpDown numericUpDownPressaoLess;
        private System.Windows.Forms.NumericUpDown numericUpDownTempLess;
        private System.Windows.Forms.CheckBox checkBoxGreaterH;
        private System.Windows.Forms.CheckBox checkBoxGreaterP;
        private System.Windows.Forms.CheckBox checkBoxGreaterT;
        private System.Windows.Forms.CheckBox checkBoxLessH;
        private System.Windows.Forms.CheckBox checkBoxLessP;
        private System.Windows.Forms.CheckBox checkBoxLessT;
        private System.Windows.Forms.CheckBox checkBoxEqualsH;
        private System.Windows.Forms.CheckBox checkBoxEqualsP;
        private System.Windows.Forms.CheckBox checkBoxEqualsT;
        private System.Windows.Forms.NumericUpDown numericUpDownHumidadeEquals;
        private System.Windows.Forms.NumericUpDown numericUpDownPressaoEquals;
        private System.Windows.Forms.NumericUpDown numericUpDownTempEquals;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

