﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace AlarmingSystem
{
    public partial class AlarmSettingsForm : Form
    {

        static private String xmlAlarmSettings = AlarmingSystem.Properties.Settings.Default.xmlAlarmSettings;
        static private String xsdAlarmSettings = AlarmingSystem.Properties.Settings.Default.xsdAlarmSettings;
            
        public AlarmSettingsForm()
        {
            InitializeComponent();

            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var errors = "";

            //VERIFICAÇÃO: less value > greater value
            if (numericUpDownTempLess.Value > numericUpDownTempGreater.Value)
                errors += "Your TEMPERATURE 'Less than' value shoud be lower than your 'Greater than' value!" + Environment.NewLine;
            if (numericUpDownPressaoLess.Value > numericUpDownPressaoGreater.Value)
                errors += "Your PRESSURE 'Less than' value shoud be lower than your 'Greater than' value!" + Environment.NewLine;
            if (numericUpDownHumidadeLess.Value > numericUpDownHumidadeGreater.Value)
                errors += "Your HUMIDITY 'Less than' value shoud be lower than your 'Greater than' value!" + Environment.NewLine;

            //VERIFICAÇÃO: greater value < less value
            if (numericUpDownTempGreater.Value < numericUpDownTempLess.Value)
                errors += "Your TEMPERATURE 'Greater than' value shoud be higher than your 'Less than' value!" + Environment.NewLine;
            if (numericUpDownPressaoGreater.Value < numericUpDownPressaoLess.Value)
                errors += "Your PRESSURE 'Greater than' value shoud be higher than your 'Less than' value!" + Environment.NewLine;
            if (numericUpDownHumidadeGreater.Value < numericUpDownHumidadeLess.Value)
                errors += "Your HUMIDITY 'Greater than' value shoud be higher than your 'Less than' value!" + Environment.NewLine;

            if(errors == "") {
                createXMLfile();
                this.Close();
            }
            else
            {
                MessageBox.Show(errors);
            }
        }


        public void createXMLfile(){
            XmlDocument doc = new XmlDocument();

            XmlDeclaration declaracao = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(declaracao);
            
            XmlElement root = doc.CreateElement("alarmSettings");
            doc.AppendChild(root);

            XmlElement sensorHumidade = CreateSensorChannel(doc, "Humidade", numericUpDownHumidadeLess.Value, numericUpDownHumidadeGreater.Value, numericUpDownHumidadeEquals.Value, checkBoxLessH.Checked, checkBoxGreaterH.Checked, checkBoxEqualsH.Checked);
            XmlElement sensorPressao = CreateSensorChannel(doc, "Pressao", numericUpDownPressaoLess.Value, numericUpDownPressaoGreater.Value, numericUpDownPressaoEquals.Value, checkBoxLessP.Checked, checkBoxGreaterP.Checked, checkBoxEqualsP.Checked);
            XmlElement sensorTemperatura = CreateSensorChannel(doc, "Temperatura", numericUpDownTempLess.Value, numericUpDownTempGreater.Value, numericUpDownTempEquals.Value, checkBoxLessT.Checked, checkBoxGreaterT.Checked, checkBoxEqualsT.Checked);


            root.AppendChild(sensorHumidade);
            root.AppendChild(sensorPressao);
            root.AppendChild(sensorTemperatura);


            doc.Save(xmlAlarmSettings);
            MessageBox.Show("File Updated Sucessed!!");
        }

        private static XmlElement CreateSensorChannel(XmlDocument doc, string categoria, decimal lessValue, decimal greaterValue, decimal equalsValue, Boolean checkBoxLess, Boolean checkBoxGreater, Boolean checkBoxEquals)
        {
            XmlElement sensorSettings = doc.CreateElement("sensor");
            sensorSettings.SetAttribute("category", categoria);

            XmlElement _lessValue = doc.CreateElement("lessValue");
            _lessValue.InnerText = lessValue.ToString();
            _lessValue.SetAttribute("enable", checkBoxLess.ToString());

            XmlElement _greaterValue = doc.CreateElement("greaterValue");
            _greaterValue.InnerText = greaterValue.ToString();
            _greaterValue.SetAttribute("enable", checkBoxGreater.ToString());

            XmlElement _equalsValue = doc.CreateElement("equalsValue");
            _equalsValue.InnerText = equalsValue.ToString();
            _equalsValue.SetAttribute("enable", checkBoxEquals.ToString());


            sensorSettings.AppendChild(_lessValue);
            sensorSettings.AppendChild(_greaterValue);
            sensorSettings.AppendChild(_equalsValue);

            return sensorSettings;
        }

        private void AlarmSettingsForm_Load(object sender, EventArgs e)
        {
            //CARREGAR OS VALORES DO XML PARA O FORMULÁRIO
            loadSettingsAlarm("Temperatura");
            loadSettingsAlarm("Pressao");
            loadSettingsAlarm("Humidade");
        }

        public void loadSettingsAlarm(String sensorType)
        {
            MyXmlHandler myHandler = new MyXmlHandler(Application.StartupPath + "\\" + xmlAlarmSettings, Application.StartupPath + "\\" + xsdAlarmSettings);

            if (File.Exists(xmlAlarmSettings) && myHandler.ValidateXml().ToString().Equals("True"))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(xmlAlarmSettings);
                XmlElement alarmSettings = (XmlElement)doc.SelectSingleNode("/alarmSettings/sensor[@category='"+sensorType+"']");
                XmlNode lessValue = alarmSettings.SelectSingleNode("lessValue");
                XmlNode greaterValue = alarmSettings.SelectSingleNode("greaterValue");
                XmlNode equalsValue = alarmSettings.SelectSingleNode("equalsValue");
                XmlAttribute lessEnable = lessValue.Attributes["enable"];
                XmlAttribute greaterEnable = greaterValue.Attributes["enable"];
                XmlAttribute equalsEnable = equalsValue.Attributes["enable"];

                var errors = "";
                //VERIFICAÇÃO: less value > greater value
                if (Decimal.Parse(lessValue.InnerText) > Decimal.Parse(greaterValue.InnerText))
                    errors += "Your "+ sensorType.ToUpper() +" 'Less than' value shoud be lower than your 'Greater than' value!" + Environment.NewLine;

                //VERIFICAÇÃO: greater value < less value
                if (Decimal.Parse(greaterValue.InnerText) < Decimal.Parse(lessValue.InnerText))
                    errors += "Your " + sensorType.ToUpper() + " 'Greater than' value shoud be higher than your 'Less than' value!" + Environment.NewLine;

                if (errors != "")
                {
                    MessageBox.Show(errors);
                    return;
                }

                //ACTUALIZAR FORM HUMIDADE
                if (sensorType == "Humidade")
                {
                    numericUpDownHumidadeLess.Value = Decimal.Parse(lessValue.InnerText);
                    numericUpDownHumidadeGreater.Value = Decimal.Parse(greaterValue.InnerText);
                    numericUpDownHumidadeEquals.Value = Decimal.Parse(equalsValue.InnerText);
                    if (lessEnable.Value.ToLower() == "false"){
                        checkBoxLessH.Checked = false;
                        numericUpDownHumidadeLess.Enabled = false;
                    }
                    else
                    {
                        checkBoxLessH.Checked = true;
                        numericUpDownHumidadeLess.Enabled = true;
                    }

                    if (greaterEnable.Value.ToLower() == "false")
                    {
                        checkBoxGreaterH.Checked = false;
                        numericUpDownHumidadeGreater.Enabled = false;
                    }
                    else
                    {
                        checkBoxGreaterH.Checked = true;
                        numericUpDownHumidadeGreater.Enabled = true;
                    }

                    if (equalsEnable.Value.ToLower() == "false")
                    {
                        checkBoxEqualsH.Checked = false;
                        numericUpDownHumidadeEquals.Enabled = false;
                    }
                    else
                    {
                        checkBoxEqualsH.Checked = true;
                        numericUpDownHumidadeEquals.Enabled = true;
                    }
                        
                }
                else if (sensorType == "Pressao")
                {
                    numericUpDownPressaoLess.Value = Decimal.Parse(lessValue.InnerText);
                    numericUpDownPressaoGreater.Value = Decimal.Parse(greaterValue.InnerText);
                    numericUpDownPressaoEquals.Value = Decimal.Parse(equalsValue.InnerText);
                    if (lessEnable.Value.ToLower() == "false")
                    {
                        checkBoxLessP.Checked = false;
                        numericUpDownPressaoLess.Enabled = false;
                    }
                    else
                    {
                        checkBoxLessP.Checked = true;
                        numericUpDownPressaoLess.Enabled = true;
                    }

                    if (greaterEnable.Value.ToLower() == "false")
                    {
                        checkBoxGreaterP.Checked = false;
                        numericUpDownPressaoGreater.Enabled = false;
                    }
                    else
                    {
                        checkBoxGreaterP.Checked = true;
                        numericUpDownPressaoGreater.Enabled = true;
                    }

                    if (equalsEnable.Value.ToLower() == "false")
                    {
                        checkBoxEqualsP.Checked = false;
                        numericUpDownPressaoEquals.Enabled = false;
                    }
                    else
                    {
                        checkBoxEqualsP.Checked = true;
                        numericUpDownPressaoEquals.Enabled = true;
                    }
                        
                }
                else if (sensorType == "Temperatura")
                {
                    numericUpDownTempLess.Value = Decimal.Parse(lessValue.InnerText);
                    numericUpDownTempGreater.Value = Decimal.Parse(greaterValue.InnerText);
                    numericUpDownTempEquals.Value = Decimal.Parse(equalsValue.InnerText);
                    if (lessEnable.Value.ToLower() == "false")
                    {
                        checkBoxLessT.Checked = false;
                        numericUpDownTempLess.Enabled = false;
                    }
                    else
                    {
                        checkBoxLessT.Checked = true;
                        numericUpDownTempLess.Enabled = true;
                    }

                    if (greaterEnable.Value.ToLower() == "false")
                    {
                        checkBoxGreaterT.Checked = false;
                        numericUpDownTempGreater.Enabled = false;
                    }
                    else
                    {
                        checkBoxGreaterT.Checked = true;
                        numericUpDownTempGreater.Enabled = true;
                    }

                    if (equalsEnable.Value.ToLower() == "false")
                    {
                        checkBoxEqualsT.Checked = false;
                        numericUpDownTempEquals.Enabled = false;
                    }
                    else
                    {
                        checkBoxEqualsT.Checked = true;
                        numericUpDownTempEquals.Enabled = true;
                    }
                        
                }
            }
 
        }

        private void checkBoxOptionTemperaturaGreater_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxGreaterT.Checked == false)
                numericUpDownTempGreater.Enabled = false;
            else
                numericUpDownTempGreater.Enabled = true;
        }

        private void checkBoxOptionPressaoGreater_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxGreaterP.Checked == false)
                numericUpDownPressaoGreater.Enabled = false;
            else
                numericUpDownPressaoGreater.Enabled = true;
        }

        private void checkBoxOptionHumidadeGreater_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxGreaterH.Checked == false)
                numericUpDownHumidadeGreater.Enabled = false;
            else
                numericUpDownHumidadeGreater.Enabled = true;
        }
        //////////
        private void checkBoxOptionTemperaturaLess_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxLessT.Checked == false)
                numericUpDownTempLess.Enabled = false;
            else
                numericUpDownTempLess.Enabled = true;
        }

        private void checkBoxOptionPressaoLess_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxLessP.Checked == false)
                numericUpDownPressaoLess.Enabled = false;
            else
                numericUpDownPressaoLess.Enabled = true;
        }

        private void checkBoxOptionHumidadeLess_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxLessH.Checked == false)
                numericUpDownHumidadeLess.Enabled = false;
            else
                numericUpDownHumidadeLess.Enabled = true;
        }
        //////// 
        private void checkBoxEqualsT_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEqualsT.Checked == false)
                numericUpDownTempEquals.Enabled = false;
            else
                numericUpDownTempEquals.Enabled = true;
        }

        private void checkBoxEqualsP_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEqualsP.Checked == false)
                numericUpDownPressaoEquals.Enabled = false;
            else
                numericUpDownPressaoEquals.Enabled = true;
        }

        private void checkBoxEqualsH_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEqualsH.Checked == false)
                numericUpDownHumidadeEquals.Enabled = false;
            else
                numericUpDownHumidadeEquals.Enabled = true;
        }


    }
}
