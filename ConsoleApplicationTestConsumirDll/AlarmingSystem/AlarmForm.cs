﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZeroMQ;
using SensorDataController;
using Newtonsoft.Json;
using System.Threading;
using System.Xml;
using System.IO;
using System.Web.Script.Serialization;


namespace AlarmingSystem
{

    public partial class AlarmForm : Form
    {
        Thread threadTemperatura;
        Thread threadPressao;
        Thread threadHumidade;

        Boolean flagStartAlarm = false;

        XmlNode lessValueT, lessValueP, lessValueH;
        XmlNode greaterValueT, greaterValueP, greaterValueH;
        XmlNode equalsValueT, equalsValueP, equalsValueH;
        XmlAttribute enableLessT, enableLessP, enableLessH;
        XmlAttribute enableGreaterT, enableGreaterP, enableGreaterH;
        XmlAttribute enableEqualsT, enableEqualsP, enableEqualsH;

        static private String xmlAlarmSettings = AlarmingSystem.Properties.Settings.Default.xmlAlarmSettings;
        static private String xsdAlarmSettings = AlarmingSystem.Properties.Settings.Default.xsdAlarmSettings;

        public static ZContext context;
        public static ZSocket publisherAlarm;

        public AlarmForm()
        {
            InitializeComponent();

            context = new ZContext();
            publisherAlarm = new ZSocket(context, ZSocketType.PUB);     //PUBLISHER RESPONSÁVEL POR ENVIAR OS ALARMES PARA QUEM QUISER OUVIR

            //Canal de Publicar Alarme
            string adressAlarmes = "tcp://*:" + AlarmingSystem.Properties.Settings.Default.portoAlarmes; // MANDA PARA TODOS QUE ESTÃO A ESCUTA NAQUELE PORTO, ISTO PORQUE SE QUISERMOS ACRESTAAR MAIS ALGUM MODULO ELE JÁ ESTÁ PREPARADO
            publisherAlarm.Bind(adressAlarmes);
        }

        public void ThreadTemperatura()
        {
            string connect_to_Temperatura = "tcp://" + AlarmingSystem.Properties.Settings.Default.pubTemperatura;
            VerificarAlarme(connect_to_Temperatura, "Temperatura");
        }

        public void ThreadPressao()
        {
            string connect_to_Pressao = "tcp://" + AlarmingSystem.Properties.Settings.Default.pubPressao;
            VerificarAlarme(connect_to_Pressao, "Pressao");
        }

        public void ThreadHumidade()
        {
            string connect_to_Humidade = "tcp://" + AlarmingSystem.Properties.Settings.Default.pubHumidade;
            VerificarAlarme(connect_to_Humidade, "Humidade");
        }


        public void VerificarAlarme(String connection, String tipoAlarme)
        {
            var context = new ZContext();
            var subscriber = new ZSocket(context, ZSocketType.SUB);
            subscriber.Connect(connection);

            this.BeginInvoke(new MethodInvoker(delegate
            {
                UpdateInterface(string.Format("Thread " + tipoAlarme + ": Connecting to {0}...", connection));
            }));


            //TESTAR EIXO H XLSX

            while (true)
            {
                subscriber.SubscribeAll();
                var replyFrame = subscriber.ReceiveFrame();

                string reply = replyFrame.ReadString();
                SensorDados sensorDados = JsonConvert.DeserializeObject<SensorDados>(reply);
                Console.WriteLine(sensorDados);

                if (tipoAlarme.Equals("Temperatura"))
                {
                    typeOfAlarmCondition(enableEqualsT, enableGreaterT, enableLessT, equalsValueT, greaterValueT, lessValueT, sensorDados);
                }
                else if (tipoAlarme.Equals("Pressao"))
                {
                    typeOfAlarmCondition(enableEqualsP, enableGreaterP, enableLessP, equalsValueP, greaterValueP, lessValueP, sensorDados);
                }
                else if (tipoAlarme.Equals("Humidade"))
                {
                    typeOfAlarmCondition(enableEqualsH, enableGreaterH, enableLessH, equalsValueH, greaterValueH, lessValueH, sensorDados);
                }

            }
        }

        private void typeOfAlarmCondition(XmlAttribute enableEquals, XmlAttribute enableGreater, XmlAttribute enableLess, XmlNode equalsValue, XmlNode greaterValue, XmlNode lessValue, SensorDados sensorDados)
        {
            //IR VERIFICAR AO XML SE ESTÁ DENTRO DOS VALORES E MANDAR ALARMES 
            if (enableLess.Value.ToLower() == "true")
            {
                double currentLessValue = Convert.ToDouble(lessValue.InnerText.Replace('.', ','));

                if (sensorDados.valor < currentLessValue) // SE O VALOR FOR MENOR DO VALOR MENOR DEFINIDO
                {
                    this.BeginInvoke(new MethodInvoker(delegate
                    {
                        UpdateInterface(sensorDados);

                        //FAZER O SUBSCIBER PARA QUEM QUISER OUVIR
                        SensorDados sendAlarme = new SensorDados(sensorDados.idSensor, sensorDados.tipo, sensorDados.valor);
                        var json = new JavaScriptSerializer().Serialize(sendAlarme);
                        var updateFrame = new ZFrame(json);
                        publisherAlarm.Send(updateFrame);

                        string alarmInfo = "Alarme Despoletado: " + sensorDados.tipo + " : " + sensorDados.valor + "  -- na data: " + sensorDados.dataTime;

                    }));
                }
            }

            if (enableGreater.Value.ToLower() == "true")
            {
                double currentGreaterValue = Convert.ToDouble(greaterValue.InnerText.Replace('.', ','));

                if (sensorDados.valor > currentGreaterValue) // SE O VALOR FOR MENOR DO VALOR MENOR DEFINIDO
                {
                    this.BeginInvoke(new MethodInvoker(delegate
                    {
                        UpdateInterface(sensorDados);

                        //FAZER O SUBSCIBER PARA QUEM QUISER OUVIR
                        SensorDados sendAlarme = new SensorDados(sensorDados.idSensor, sensorDados.tipo, sensorDados.valor);
                        var json = new JavaScriptSerializer().Serialize(sendAlarme);
                        var updateFrame = new ZFrame(json);
                        publisherAlarm.Send(updateFrame);

                        string alarmInfo = "Alarme Despoletado: " + sensorDados.tipo + " : " + sensorDados.valor + "  -- na data: " + sensorDados.dataTime;

                    }));
                }
            }

            if (enableEquals.Value.ToLower() == "true")
            {
                double currentEqualsValue = Convert.ToDouble(equalsValue.InnerText.Replace('.', ','));

                if (sensorDados.valor == currentEqualsValue) // SE O VALOR FOR MENOR DO VALOR MENOR DEFINIDO
                {
                    this.BeginInvoke(new MethodInvoker(delegate
                    {
                        UpdateInterface(sensorDados);

                        //FAZER O SUBSCIBER PARA QUEM QUISER OUVIR
                        SensorDados sendAlarme = new SensorDados(sensorDados.idSensor, sensorDados.tipo, sensorDados.valor);
                        var json = new JavaScriptSerializer().Serialize(sendAlarme);
                        var updateFrame = new ZFrame(json);
                        publisherAlarm.Send(updateFrame);

                        string alarmInfo = "Alarme Despoletado: " + sensorDados.tipo + " : " + sensorDados.valor + "  -- na data: " + sensorDados.dataTime;

                    }));
                }
            }
        }





        private void UpdateInterface(SensorDados sensorDados)
        {
            lstAlarms.Items.Add("Alarme Despoletado: " + sensorDados.tipo + " : " + sensorDados.valor + "  -- na data: " + sensorDados.dataTime);
        }

        private void UpdateInterface(string info)
        {
            lstAlarms.Items.Add(info);
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            AlarmSettingsForm alarmButton = new AlarmSettingsForm();
            alarmButton.ShowDialog();
        }

        public void getConditionsForAlarm()
        {
            if (File.Exists(xmlAlarmSettings))
            {
                MyXmlHandler myHandler = new MyXmlHandler(Application.StartupPath + "\\" + xmlAlarmSettings, Application.StartupPath + "\\" + xsdAlarmSettings);

                if (myHandler.ValidateXml().ToString().Equals("True"))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlAlarmSettings);
                    XmlElement alarmSettings = (XmlElement)doc.SelectSingleNode("/alarmSettings");

                    XmlElement alarmeT = (XmlElement)alarmSettings.SelectSingleNode("sensor[@category='Temperatura']");
                    lessValueT = alarmeT.SelectSingleNode("lessValue");
                    greaterValueT = alarmeT.SelectSingleNode("greaterValue");
                    equalsValueT = alarmeT.SelectSingleNode("equalsValue");
                    enableLessT = lessValueT.Attributes["enable"];
                    enableGreaterT = greaterValueT.Attributes["enable"];
                    enableEqualsT = equalsValueT.Attributes["enable"];

                    XmlElement alarmeP = (XmlElement)alarmSettings.SelectSingleNode("sensor[@category='Pressao']");
                    lessValueP = alarmeP.SelectSingleNode("lessValue");
                    greaterValueP = alarmeP.SelectSingleNode("greaterValue");
                    equalsValueP = alarmeP.SelectSingleNode("equalsValue");
                    enableLessP = lessValueP.Attributes["enable"];
                    enableGreaterP = greaterValueP.Attributes["enable"];
                    enableEqualsP = equalsValueP.Attributes["enable"];

                    XmlElement alarmeH = (XmlElement)alarmSettings.SelectSingleNode("sensor[@category='Humidade']");
                    lessValueH = alarmeH.SelectSingleNode("lessValue");
                    greaterValueH = alarmeH.SelectSingleNode("greaterValue");
                    equalsValueH = alarmeH.SelectSingleNode("equalsValue");
                    enableLessH = lessValueH.Attributes["enable"];
                    enableGreaterH = greaterValueH.Attributes["enable"];
                    enableEqualsH = equalsValueH.Attributes["enable"];


                    var errors = "";

                    //VERIFICAÇÃO TEMPERATURA: less value > greater value
                    if (convertToDouble(lessValueT) > convertToDouble(greaterValueT))
                        errors += "Your TEMPERATURE 'Less than' value shoud be lower than your 'Greater than' value!" + Environment.NewLine;
                    //VERIFICAÇÃ TEMPERATURAO: greater value < less value
                    if (convertToDouble(greaterValueT) < convertToDouble(lessValueT))
                        errors += "Your TEMPERATURE 'Greater than' value shoud be higher than your 'Less than' value!" + Environment.NewLine;


                    //VERIFICAÇÃO PRESSAO: less value > greater value
                    if (convertToDouble(lessValueP) > convertToDouble(greaterValueP))
                        errors += "Your PRESSURE 'Less than' value shoud be lower than your 'Greater than' value!" + Environment.NewLine;
                    //VERIFICAÇÃO PRESSAO: greater value < less value
                    if (convertToDouble(greaterValueP) < convertToDouble(lessValueP))
                        errors += "Your PRESSURE 'Greater than' value shoud be higher than your 'Less than' value!" + Environment.NewLine;


                    //VERIFICAÇÃO HUMIDADE: less value > greater value
                    if (convertToDouble(lessValueH) > convertToDouble(greaterValueH))
                        errors += "Your HUMIDITY 'Less than' value shoud be lower than your 'Greater than' value!" + Environment.NewLine;
                    //VERIFICAÇÃO HUMIDADE: greater value < less value
                    if (convertToDouble(greaterValueH) < convertToDouble(lessValueH))
                        errors += "Your HUMIDITY 'Greater than' value shoud be higher than your 'Less than' value!" + Environment.NewLine;

                    if (errors != "")
                    {
                        lstAlarms.Items.Clear();
                        btnSettings.Enabled = true;
                        MessageBox.Show(errors);

                        //Aborta as threads para o caso de já estarem a correr...
                        if (threadTemperatura != null)
                        {
                            threadTemperatura.Abort();
                            threadTemperatura = null;
                        }
                        if (threadHumidade != null)
                        {
                            threadHumidade.Abort();
                            threadHumidade = null;
                        }
                        if (threadPressao != null)
                        {
                            threadPressao.Abort();
                            threadPressao = null;
                        }
                    }
                }
            }
        }

        public double convertToDouble(XmlNode value)
        {
            double currentValue = Convert.ToDouble(value.InnerText.Replace('.', ','));
            return currentValue;
        }

        public void btnStart_Click(object sender, EventArgs e)
        {
            //Lê ficheiro XML das Settings e guarda os valores em memória.
            getConditionsForAlarm();

            flagStartAlarm = !flagStartAlarm;

            //Valida XML das Settings com o seu XSD
            
                MyXmlHandler myHandler = new MyXmlHandler(Application.StartupPath + "\\" + xmlAlarmSettings, Application.StartupPath + "\\" + xsdAlarmSettings);

                if (myHandler.ValidateXml().ToString().Equals("True"))
                {
                    if (flagStartAlarm)
                    {
                        try
                        {
                            threadTemperatura = new Thread(ThreadTemperatura);
                            threadTemperatura.Start();
                            threadPressao = new Thread(ThreadPressao);
                            threadPressao.Start();
                            threadHumidade = new Thread(ThreadHumidade);
                            threadHumidade.Start();
                            lstAlarms.Items.Clear();
                            btnStart.Text = "Stop Alarm System";
                            btnSettings.Enabled = false;
                        }
                        catch (ThreadStateException ex1)
                        {
                            MessageBox.Show("At this time is not possible to start the alarm system. Please try again later...");
                        }
                    }
                    else
                    {
                        btnStart.Text = "Start Alarm System";
                        btnSettings.Enabled = true;
                        threadTemperatura.Abort();
                        threadTemperatura = null;
                        threadPressao.Abort();
                        threadPressao = null;
                        threadHumidade.Abort();
                        threadHumidade = null;
                    }

                }
                else
                {
                    MessageBox.Show("The XML file of Alarm Settings has been deleted or is corrupted. " + Environment.NewLine + myHandler.ValidateXml().ToString() + " -->" + myHandler.ValidateMessage);
                }

                Console.WriteLine("Threads: Worker threads has terminated.");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (threadTemperatura != null)
            {
                threadTemperatura.Abort();
                threadTemperatura = null;
            }
            if (threadHumidade != null)
            {
                threadHumidade.Abort();
                threadHumidade = null;
            }
            if (threadPressao != null)
            {
                threadPressao.Abort();
                threadPressao = null;
            }

            this.Hide();
        }


    }

}
