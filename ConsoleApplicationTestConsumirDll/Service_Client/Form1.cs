﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Service_Client.ServerClient;

namespace Service_Client
{
    public partial class Form1 : Form
    {
        ServerClient.Service1Client service = null;
        Service_Client.ServerClient.SensorDados[] sensors;
        Service_Client.ServerClient.StatisticValues statisticValuesT;
        Service_Client.ServerClient.StatisticValues statisticValuesP;
        Service_Client.ServerClient.StatisticValues statisticValuesH;

        String[] sensorsDescription;
        

        public Form1()
        {
            InitializeComponent();
            dateTimeStart.Format = DateTimePickerFormat.Custom;
            dateTimeStart.CustomFormat = "dd-MM-yyyy HH:mm:ss";
            
            dateTimeEnd.Format = DateTimePickerFormat.Custom;
            dateTimeEnd.CustomFormat = "dd-MM-yyyy HH:mm:ss";

            var now = DateTime.Now;
            dateTimeStart.Value = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            service = new ServerClient.Service1Client();
        }


        private string getFormatedDatePicker(DateTimePicker datePicker) //FORMATAR VALOR QUE VEM DO DATAPICKER
        {
            DateTime dateTime = datePicker.Value;
            return dateTime.ToString("yyyy/MM/dd HH:mm:ss");
        }

        private string sensorToString(Service_Client.ServerClient.SensorDados sensor)
        {
            return "Sensor ID: " + sensor.IdSensor + "  Type Sensor: " + sensor.TipoSensor + " Value:  " + sensor.Valor + " Date:  " + sensor.Data;
        }


        private void btnGetResults_Click(object sender, EventArgs e)
        {
            //btnGetResults.Enabled = false;

            if (!chkBoxTemp.Checked && !chkBoxPressure.Checked && !chkBoxHumidity.Checked)
            {
                MessageBox.Show("You have to choose at least one sensor to get data!");
                //btnGetResults.Enabled = true;
            }

            if (dateTimeEnd.Value < dateTimeStart.Value)            //0.o DEVIA DE SER <= PARA VALIDAR AS DADAS IGUAIS, ASSIM JÁ NÁO ESTOIRAVA
            {
                MessageBox.Show("The end date has to be greater than the start date ", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //btnGetResults.Enabled = true;
            }
            else
            {
                if (chkBoxTemp.Checked && !chkBoxPressure.Checked && !chkBoxHumidity.Checked)
                {
                    getIntervalDate('T');
                    groupBoxTemp.Enabled = true;
                    groupBoxPressure.Enabled = false;
                    groupBoxHumidity.Enabled = false;
                    lblAvgP.Text = "-";
                    lblAvgH.Text = "-";
                    lblMaxP.Text = "-";
                    lblMaxH.Text = "-";
                    lblMinP.Text = "-";
                    lblMinH.Text = "-";
                }

                else if (!chkBoxTemp.Checked && chkBoxPressure.Checked && !chkBoxHumidity.Checked)
                {
                    getIntervalDate('P');
                    groupBoxTemp.Enabled = false;
                    groupBoxPressure.Enabled = true;
                    groupBoxHumidity.Enabled = false;
                    lblAvgT.Text = "-";
                    lblAvgH.Text = "-";
                    lblMaxT.Text = "-";
                    lblMaxH.Text = "-";
                    lblMinT.Text = "-";
                    lblMinH.Text = "-";
                }

                else if (!chkBoxTemp.Checked && !chkBoxPressure.Checked && chkBoxHumidity.Checked)
                {
                    getIntervalDate('H');
                    groupBoxTemp.Enabled = false;
                    groupBoxPressure.Enabled = false;
                    groupBoxHumidity.Enabled = true;
                    lblAvgP.Text = "-";
                    lblAvgT.Text = "-";
                    lblMaxP.Text = "-";
                    lblMaxT.Text = "-";
                    lblMinP.Text = "-";
                    lblMinT.Text = "-";
                }

                else if (chkBoxTemp.Checked && chkBoxPressure.Checked && !chkBoxHumidity.Checked)
                {
                    GetIntervalDate2Sensors('T', 'P');
                    groupBoxTemp.Enabled = true;
                    groupBoxPressure.Enabled = true;
                    groupBoxHumidity.Enabled = false;
                    lblAvgH.Text = "-";
                    lblMaxH.Text = "-";
                    lblMinH.Text = "-";
                }

                else if (chkBoxTemp.Checked && !chkBoxPressure.Checked && chkBoxHumidity.Checked)
                {
                    GetIntervalDate2Sensors('T', 'H');
                    groupBoxTemp.Enabled = true;
                    groupBoxPressure.Enabled = false;
                    groupBoxHumidity.Enabled = true;
                    lblAvgP.Text = "-";
                    lblMaxP.Text = "-";
                    lblMinP.Text = "-";
                }

                else if (!chkBoxTemp.Checked && chkBoxPressure.Checked && chkBoxHumidity.Checked)
                {
                    GetIntervalDate2Sensors('P', 'H');
                    groupBoxTemp.Enabled = false;
                    groupBoxPressure.Enabled = true;
                    groupBoxHumidity.Enabled = true;
                    lblAvgT.Text = "-";
                    lblMaxT.Text = "-";
                    lblMinT.Text = "-";
                }
                else if (chkBoxTemp.Checked && chkBoxPressure.Checked && chkBoxHumidity.Checked)
                {
                    GetIntervalDate3Sensors('P', 'H', 'T');
                    groupBoxTemp.Enabled = true;
                    groupBoxPressure.Enabled = true;
                    groupBoxHumidity.Enabled = true;
                }
            }

            
        }

        private void getIntervalDate(char typeSensor)
        {
            try
            {
                sensors = service.GetIntervalDate(typeSensor, getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                //btnGetResults.Enabled = true;
                
                
            }
            catch (Exception)
            {
                MessageBox.Show("The query execeeds max number of records allowed! \n Please redefine your parameters! ", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //REVER POR CAUSA DO CHECKLISTITEMS
            sensorsDescription = Array.ConvertAll<Service_Client.ServerClient.SensorDados, string>(sensors, sensorToString);
            listBoxResults.DataSource = sensorsDescription;
            labelTotalRegists.Text = sensors.Count().ToString();

            if (sensors.Count() != 0)
            {
                btnExportExcel.Enabled = true;
            }
            else { btnExportExcel.Enabled = false; }

            switch (typeSensor)
            {
                case 'T': 
                          statisticValuesT = service.GetStatisticIntervalDate('T', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                          if (statisticValuesT != null) { 
                              lblAvgT.Text = statisticValuesT.Average.ToString();
                              lblMaxT.Text = statisticValuesT.MaxValue.ToString() + " ºC";
                              lblMinT.Text = statisticValuesT.MinValue.ToString() + " ºC";
                          }
                    break;
                case 'H':
                          statisticValuesH = service.GetStatisticIntervalDate('H', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                          if (statisticValuesH != null)
                          {
                              lblAvgH.Text = statisticValuesH.Average.ToString();
                              lblMaxH.Text = statisticValuesH.MaxValue.ToString();
                              lblMinH.Text = statisticValuesH.MinValue.ToString();
                          }
                    break;
                case 'P':
                          statisticValuesP = service.GetStatisticIntervalDate('P', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                          if (statisticValuesP != null)
                          {
                              lblAvgP.Text = statisticValuesP.Average.ToString();
                              lblMaxP.Text = statisticValuesP.MaxValue.ToString();
                              lblMinP.Text = statisticValuesP.MinValue.ToString();
                          }
                    break;
            }

        }

        private void GetIntervalDate2Sensors(char typeSensor, char typeSensor2)
        {
            try
            {
                sensors = service.GetIntervalDate2Sensors(typeSensor, typeSensor2, getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                //btnGetResults.Enabled = true;
            }
            catch (Exception)
            {
                MessageBox.Show("The query execeeds max number of records allowed! \n Please redefine your parameters! ", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //REVER POR CAUSA DO CHECKLISTITEMS
            sensorsDescription = Array.ConvertAll<Service_Client.ServerClient.SensorDados, string>(sensors, sensorToString);
            listBoxResults.DataSource = sensorsDescription;
            labelTotalRegists.Text = sensors.Count().ToString();

            if (sensors.Count() != 0)
            {
                btnExportExcel.Enabled = true;
            }
            else { btnExportExcel.Enabled = false; }

            if(typeSensor == 'T' && typeSensor2 == 'P')
            {
                statisticValuesT = service.GetStatisticIntervalDate('T', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                statisticValuesP = service.GetStatisticIntervalDate('P', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                if (statisticValuesT != null && statisticValuesP != null)
                {
                    lblAvgT.Text = statisticValuesT.Average.ToString();
                    lblMaxT.Text = statisticValuesT.MaxValue.ToString() + " ºC";
                    lblMinT.Text = statisticValuesT.MinValue.ToString() + " ºC";

                    lblAvgP.Text = statisticValuesP.Average.ToString();
                    lblMaxP.Text = statisticValuesP.MaxValue.ToString();
                    lblMinP.Text = statisticValuesP.MinValue.ToString();
                }
            }
            else if (typeSensor == 'T' && typeSensor2 == 'H')
            {
                statisticValuesT = service.GetStatisticIntervalDate('T', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                statisticValuesH = service.GetStatisticIntervalDate('H', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                if (statisticValuesT != null && statisticValuesH != null)
                {
                    lblAvgT.Text = statisticValuesT.Average.ToString();
                    lblMaxT.Text = statisticValuesT.MaxValue.ToString() + " ºC";
                    lblMinT.Text = statisticValuesT.MinValue.ToString() + " ºC";

                    lblAvgH.Text = statisticValuesH.Average.ToString();
                    lblMaxH.Text = statisticValuesH.MaxValue.ToString();
                    lblMinH.Text = statisticValuesH.MinValue.ToString();
                }
            }
            else if (typeSensor == 'P' && typeSensor2 == 'H')
            {
                statisticValuesP = service.GetStatisticIntervalDate('P', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                statisticValuesH = service.GetStatisticIntervalDate('H', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                if (statisticValuesP != null && statisticValuesH != null)
                {
                    lblAvgP.Text = statisticValuesP.Average.ToString();
                    lblMaxP.Text = statisticValuesP.MaxValue.ToString();
                    lblMinP.Text = statisticValuesP.MinValue.ToString();

                    lblAvgH.Text = statisticValuesH.Average.ToString();
                    lblMaxH.Text = statisticValuesH.MaxValue.ToString();
                    lblMinH.Text = statisticValuesH.MinValue.ToString();
                }
            }

        }

        private void GetIntervalDate3Sensors(char typeSensor, char typeSensor2, char typeSensor3)
        {
            try
            {
                sensors = service.GetIntervalDate3Sensors(typeSensor, typeSensor2, typeSensor3, getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
                //btnGetResults.Enabled = true;
            }
            catch (Exception)
            {
                MessageBox.Show("The query execeeds max number of records allowed! \n Please redefine your parameters! ", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            //REVER POR CAUSA DO CHECKLISTITEMS
            sensorsDescription = Array.ConvertAll<Service_Client.ServerClient.SensorDados, string>(sensors, sensorToString);
            listBoxResults.DataSource = sensorsDescription;
            labelTotalRegists.Text = sensors.Count().ToString();

            if (sensors.Count() != 0)
            {
                btnExportExcel.Enabled = true;
            }
            else { btnExportExcel.Enabled = false; }

            statisticValuesP = service.GetStatisticIntervalDate('P', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
            statisticValuesH = service.GetStatisticIntervalDate('H', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));
            statisticValuesT = service.GetStatisticIntervalDate('T', getFormatedDatePicker(dateTimeStart), getFormatedDatePicker(dateTimeEnd));

            if (statisticValuesP != null && statisticValuesH != null && statisticValuesT != null)
            {
                lblAvgP.Text = statisticValuesP.Average.ToString();
                lblMaxP.Text = statisticValuesP.MaxValue.ToString();
                lblMinP.Text = statisticValuesP.MinValue.ToString();

                lblAvgH.Text = statisticValuesH.Average.ToString();
                lblMaxH.Text = statisticValuesH.MaxValue.ToString();
                lblMinH.Text = statisticValuesH.MinValue.ToString();

                lblAvgT.Text = statisticValuesT.Average.ToString();
                lblMaxT.Text = statisticValuesT.MaxValue.ToString() + " ºC";
                lblMinT.Text = statisticValuesT.MinValue.ToString() + " ºC";
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            String timeStamp = DateTime.Now.ToString() ;
            String path = Application.StartupPath + @"\dataSensorExcel.xlsx";
            ExcelHandler.CreateNewExcelFile(path);
            ExcelHandler.WriteToExcelFile(path, sensors, statisticValuesT, statisticValuesP, statisticValuesH);
            ExcelHandler.CreateChart(path, sensors.Length);
        }





    }
}
