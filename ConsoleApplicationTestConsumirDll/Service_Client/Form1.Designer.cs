﻿namespace Service_Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkBoxHumidity = new System.Windows.Forms.CheckBox();
            this.chkBoxPressure = new System.Windows.Forms.CheckBox();
            this.chkBoxTemp = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnGetResults = new System.Windows.Forms.Button();
            this.dateTimeStart = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelTotalRegists = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxResults = new System.Windows.Forms.ListBox();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.groupBoxTemp = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblMinT = new System.Windows.Forms.Label();
            this.lblMaxT = new System.Windows.Forms.Label();
            this.lblAvgT = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBoxHumidity = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblMinH = new System.Windows.Forms.Label();
            this.lblMaxH = new System.Windows.Forms.Label();
            this.lblAvgH = new System.Windows.Forms.Label();
            this.groupBoxPressure = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblMinP = new System.Windows.Forms.Label();
            this.lblMaxP = new System.Windows.Forms.Label();
            this.lblAvgP = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBoxTemp.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBoxHumidity.SuspendLayout();
            this.groupBoxPressure.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkBoxHumidity);
            this.groupBox1.Controls.Add(this.chkBoxPressure);
            this.groupBox1.Controls.Add(this.chkBoxTemp);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btnGetResults);
            this.groupBox1.Controls.Add(this.dateTimeStart);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.dateTimeEnd);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(22, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(668, 135);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parameters";
            // 
            // chkBoxHumidity
            // 
            this.chkBoxHumidity.AutoSize = true;
            this.chkBoxHumidity.Location = new System.Drawing.Point(37, 91);
            this.chkBoxHumidity.Name = "chkBoxHumidity";
            this.chkBoxHumidity.Size = new System.Drawing.Size(66, 17);
            this.chkBoxHumidity.TabIndex = 9;
            this.chkBoxHumidity.Text = "Humidity";
            this.chkBoxHumidity.UseVisualStyleBackColor = true;
            // 
            // chkBoxPressure
            // 
            this.chkBoxPressure.AutoSize = true;
            this.chkBoxPressure.Location = new System.Drawing.Point(37, 68);
            this.chkBoxPressure.Name = "chkBoxPressure";
            this.chkBoxPressure.Size = new System.Drawing.Size(67, 17);
            this.chkBoxPressure.TabIndex = 8;
            this.chkBoxPressure.Text = "Pressure";
            this.chkBoxPressure.UseVisualStyleBackColor = true;
            // 
            // chkBoxTemp
            // 
            this.chkBoxTemp.AutoSize = true;
            this.chkBoxTemp.Location = new System.Drawing.Point(37, 45);
            this.chkBoxTemp.Name = "chkBoxTemp";
            this.chkBoxTemp.Size = new System.Drawing.Size(86, 17);
            this.chkBoxTemp.TabIndex = 7;
            this.chkBoxTemp.Text = "Temperature";
            this.chkBoxTemp.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Sensors:";
            // 
            // btnGetResults
            // 
            this.btnGetResults.Location = new System.Drawing.Point(524, 38);
            this.btnGetResults.Name = "btnGetResults";
            this.btnGetResults.Size = new System.Drawing.Size(119, 74);
            this.btnGetResults.TabIndex = 1;
            this.btnGetResults.Text = "Get Results";
            this.btnGetResults.UseVisualStyleBackColor = true;
            this.btnGetResults.Click += new System.EventHandler(this.btnGetResults_Click);
            // 
            // dateTimeStart
            // 
            this.dateTimeStart.Location = new System.Drawing.Point(273, 41);
            this.dateTimeStart.Name = "dateTimeStart";
            this.dateTimeStart.Size = new System.Drawing.Size(230, 20);
            this.dateTimeStart.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(284, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Start Date";
            // 
            // dateTimeEnd
            // 
            this.dateTimeEnd.Location = new System.Drawing.Point(273, 90);
            this.dateTimeEnd.Name = "dateTimeEnd";
            this.dateTimeEnd.Size = new System.Drawing.Size(230, 20);
            this.dateTimeEnd.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(284, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "End Date";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.labelTotalRegists);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.listBoxResults);
            this.groupBox2.Location = new System.Drawing.Point(22, 178);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(668, 201);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Results List";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(524, 176);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Total of:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(597, 176);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "regists";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelTotalRegists
            // 
            this.labelTotalRegists.AutoSize = true;
            this.labelTotalRegists.Location = new System.Drawing.Point(575, 176);
            this.labelTotalRegists.Name = "labelTotalRegists";
            this.labelTotalRegists.Size = new System.Drawing.Size(13, 13);
            this.labelTotalRegists.TabIndex = 7;
            this.labelTotalRegists.Text = "0";
            this.labelTotalRegists.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(434, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 11;
            // 
            // listBoxResults
            // 
            this.listBoxResults.FormattingEnabled = true;
            this.listBoxResults.Location = new System.Drawing.Point(22, 30);
            this.listBoxResults.Name = "listBoxResults";
            this.listBoxResults.Size = new System.Drawing.Size(621, 134);
            this.listBoxResults.TabIndex = 5;
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Enabled = false;
            this.btnExportExcel.Location = new System.Drawing.Point(438, 556);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(227, 59);
            this.btnExportExcel.TabIndex = 6;
            this.btnExportExcel.Text = "Export to Excel (.xlsx)";
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // groupBoxTemp
            // 
            this.groupBoxTemp.Controls.Add(this.label4);
            this.groupBoxTemp.Controls.Add(this.label1);
            this.groupBoxTemp.Controls.Add(this.label3);
            this.groupBoxTemp.Controls.Add(this.label5);
            this.groupBoxTemp.Controls.Add(this.lblMinT);
            this.groupBoxTemp.Controls.Add(this.lblMaxT);
            this.groupBoxTemp.Controls.Add(this.lblAvgT);
            this.groupBoxTemp.Enabled = false;
            this.groupBoxTemp.Location = new System.Drawing.Point(22, 19);
            this.groupBoxTemp.Name = "groupBoxTemp";
            this.groupBoxTemp.Size = new System.Drawing.Size(159, 116);
            this.groupBoxTemp.TabIndex = 14;
            this.groupBoxTemp.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Temperature:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Minimum:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Maximum:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Average";
            // 
            // lblMinT
            // 
            this.lblMinT.AutoSize = true;
            this.lblMinT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinT.Location = new System.Drawing.Point(77, 44);
            this.lblMinT.Name = "lblMinT";
            this.lblMinT.Size = new System.Drawing.Size(11, 15);
            this.lblMinT.TabIndex = 13;
            this.lblMinT.Text = "-";
            // 
            // lblMaxT
            // 
            this.lblMaxT.AutoSize = true;
            this.lblMaxT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxT.Location = new System.Drawing.Point(77, 69);
            this.lblMaxT.Name = "lblMaxT";
            this.lblMaxT.Size = new System.Drawing.Size(11, 15);
            this.lblMaxT.TabIndex = 14;
            this.lblMaxT.Text = "-";
            // 
            // lblAvgT
            // 
            this.lblAvgT.AutoSize = true;
            this.lblAvgT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvgT.Location = new System.Drawing.Point(77, 93);
            this.lblAvgT.Name = "lblAvgT";
            this.lblAvgT.Size = new System.Drawing.Size(11, 15);
            this.lblAvgT.TabIndex = 15;
            this.lblAvgT.Text = "-";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBoxHumidity);
            this.groupBox3.Controls.Add(this.groupBoxPressure);
            this.groupBox3.Controls.Add(this.groupBoxTemp);
            this.groupBox3.Location = new System.Drawing.Point(22, 398);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(668, 141);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Statistics Results";
            // 
            // groupBoxHumidity
            // 
            this.groupBoxHumidity.Controls.Add(this.label6);
            this.groupBoxHumidity.Controls.Add(this.label11);
            this.groupBoxHumidity.Controls.Add(this.label16);
            this.groupBoxHumidity.Controls.Add(this.label17);
            this.groupBoxHumidity.Controls.Add(this.lblMinH);
            this.groupBoxHumidity.Controls.Add(this.lblMaxH);
            this.groupBoxHumidity.Controls.Add(this.lblAvgH);
            this.groupBoxHumidity.Enabled = false;
            this.groupBoxHumidity.Location = new System.Drawing.Point(484, 19);
            this.groupBoxHumidity.Name = "groupBoxHumidity";
            this.groupBoxHumidity.Size = new System.Drawing.Size(159, 116);
            this.groupBoxHumidity.TabIndex = 17;
            this.groupBoxHumidity.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Humidity:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Minimum:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 71);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 11;
            this.label16.Text = "Maximum:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 95);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "Average";
            // 
            // lblMinH
            // 
            this.lblMinH.AutoSize = true;
            this.lblMinH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinH.Location = new System.Drawing.Point(77, 44);
            this.lblMinH.Name = "lblMinH";
            this.lblMinH.Size = new System.Drawing.Size(11, 15);
            this.lblMinH.TabIndex = 13;
            this.lblMinH.Text = "-";
            // 
            // lblMaxH
            // 
            this.lblMaxH.AutoSize = true;
            this.lblMaxH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxH.Location = new System.Drawing.Point(77, 69);
            this.lblMaxH.Name = "lblMaxH";
            this.lblMaxH.Size = new System.Drawing.Size(11, 15);
            this.lblMaxH.TabIndex = 14;
            this.lblMaxH.Text = "-";
            // 
            // lblAvgH
            // 
            this.lblAvgH.AutoSize = true;
            this.lblAvgH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvgH.Location = new System.Drawing.Point(77, 93);
            this.lblAvgH.Name = "lblAvgH";
            this.lblAvgH.Size = new System.Drawing.Size(11, 15);
            this.lblAvgH.TabIndex = 15;
            this.lblAvgH.Text = "-";
            // 
            // groupBoxPressure
            // 
            this.groupBoxPressure.Controls.Add(this.label10);
            this.groupBoxPressure.Controls.Add(this.label14);
            this.groupBoxPressure.Controls.Add(this.label15);
            this.groupBoxPressure.Controls.Add(this.label19);
            this.groupBoxPressure.Controls.Add(this.lblMinP);
            this.groupBoxPressure.Controls.Add(this.lblMaxP);
            this.groupBoxPressure.Controls.Add(this.lblAvgP);
            this.groupBoxPressure.Enabled = false;
            this.groupBoxPressure.Location = new System.Drawing.Point(251, 19);
            this.groupBoxPressure.Name = "groupBoxPressure";
            this.groupBoxPressure.Size = new System.Drawing.Size(159, 116);
            this.groupBoxPressure.TabIndex = 16;
            this.groupBoxPressure.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 16);
            this.label10.TabIndex = 5;
            this.label10.Text = "Pressure:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 46);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Minimum:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 71);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "Maximum:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 95);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "Average";
            // 
            // lblMinP
            // 
            this.lblMinP.AutoSize = true;
            this.lblMinP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinP.Location = new System.Drawing.Point(77, 44);
            this.lblMinP.Name = "lblMinP";
            this.lblMinP.Size = new System.Drawing.Size(11, 15);
            this.lblMinP.TabIndex = 13;
            this.lblMinP.Text = "-";
            // 
            // lblMaxP
            // 
            this.lblMaxP.AutoSize = true;
            this.lblMaxP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxP.Location = new System.Drawing.Point(77, 69);
            this.lblMaxP.Name = "lblMaxP";
            this.lblMaxP.Size = new System.Drawing.Size(11, 15);
            this.lblMaxP.TabIndex = 14;
            this.lblMaxP.Text = "-";
            // 
            // lblAvgP
            // 
            this.lblAvgP.AutoSize = true;
            this.lblAvgP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvgP.Location = new System.Drawing.Point(77, 93);
            this.lblAvgP.Name = "lblAvgP";
            this.lblAvgP.Size = new System.Drawing.Size(11, 15);
            this.lblAvgP.TabIndex = 15;
            this.lblAvgP.Text = "-";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 627);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnExportExcel);
            this.Name = "Form1";
            this.Text = "Service Client";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBoxTemp.ResumeLayout(false);
            this.groupBoxTemp.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBoxHumidity.ResumeLayout(false);
            this.groupBoxHumidity.PerformLayout();
            this.groupBoxPressure.ResumeLayout(false);
            this.groupBoxPressure.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.ListBox listBoxResults;
        private System.Windows.Forms.Button btnGetResults;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dateTimeEnd;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimeStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelTotalRegists;
        private System.Windows.Forms.CheckBox chkBoxTemp;
        private System.Windows.Forms.CheckBox chkBoxHumidity;
        private System.Windows.Forms.CheckBox chkBoxPressure;
        private System.Windows.Forms.GroupBox groupBoxTemp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblMinT;
        private System.Windows.Forms.Label lblMaxT;
        private System.Windows.Forms.Label lblAvgT;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBoxHumidity;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblMinH;
        private System.Windows.Forms.Label lblMaxH;
        private System.Windows.Forms.Label lblAvgH;
        private System.Windows.Forms.GroupBox groupBoxPressure;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblMinP;
        private System.Windows.Forms.Label lblMaxP;
        private System.Windows.Forms.Label lblAvgP;

    }
}

