﻿using Service_Client.ServerClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace Service_Client
{
    class ExcelHandler
    {
        public static void CreateNewExcelFile(string path)
        {
            var excelApplication = new Excel.Application();
            excelApplication.Visible = false;

            var excelWorkbook = excelApplication.Workbooks.Add();

            excelWorkbook.SaveAs(path, AccessMode: Excel.XlSaveAsAccessMode.xlNoChange);
            excelWorkbook.Close();
            excelApplication.Quit();

            ReleaseComObject(excelWorkbook);
            ReleaseComObject(excelApplication);
        }

        //falta escrever as medias e para isso nao é preciso graficos
        public static void WriteToExcelFile(string path, SensorDados[] sensorlist, Service_Client.ServerClient.StatisticValues statisticsT, Service_Client.ServerClient.StatisticValues statisticsP, Service_Client.ServerClient.StatisticValues statisticsH)
         {
             Excel.Application excelApplication = new Excel.Application();
             excelApplication.Visible = false;

             Excel.Workbook excelWorkbook = excelApplication.Workbooks.Open(path);

             Excel.Worksheet excelWorksheet = excelWorkbook.ActiveSheet;
             //columns
             excelWorksheet.Cells[1, 2].Value = "Temperature";
             excelWorksheet.Cells[1, 3].Value = "Pressure";
             excelWorksheet.Cells[1, 4].Value = "Humidity";

             excelWorksheet.Cells[2, 1].Value = "Average";

             excelWorksheet.Cells[2, 2].Value = "1T";
             excelWorksheet.Cells[2, 3].Value = "2P";
             excelWorksheet.Cells[2, 4].Value = "3H";

             //Excel.Worksheet excelWorksheet2 = excelWorkbook.Worksheets[2]; // folha 2 existe?
             //Excel.Worksheet excelWorksheet2 = excelWorkbook.Worksheets.get_Item(2); // folha 2 existe?
             //Excel.Worksheet excelWorksheet2 = excelWorkbook.Worksheets.Add(); // folha 2 existe?
             //excelWorksheet2.Cells[1, 1].Value = "aaa";

             int i = 0;
             foreach (SensorDados s in sensorlist)
             {
                 excelApplication.Cells[i + 2, 1] = s.Data; 
                 if(s.TipoSensor == "T") {
                    excelApplication.Cells[i + 2, 2] = s.Valor;
                    excelApplication.Cells[i + 2, 3] = "";
                    excelApplication.Cells[i + 2, 4] = "";
                 }
                 else if (s.TipoSensor == "P")
                 {
                     excelApplication.Cells[i + 2, 2] = "";
                     excelApplication.Cells[i + 2, 3] = s.Valor;
                     excelApplication.Cells[i + 2, 4] = "";
                 }
                 else if (s.TipoSensor == "H")
                 {
                     excelApplication.Cells[i + 2, 2] = "";
                     excelApplication.Cells[i + 2, 3] = "";
                     excelApplication.Cells[i + 2, 4] = s.Valor;
                 }
                 i++;
             }

             excelApplication.Cells[i + 2, 1] = "Minimum";
             excelApplication.Cells[i + 2, 2] = statisticsT.MinValue;
             excelApplication.Cells[i + 2, 3] = statisticsP.MinValue;
             excelApplication.Cells[i + 2, 4] = statisticsH.MinValue;
            i++;

            excelApplication.Cells[i + 2, 1] = "Maximum";
            excelApplication.Cells[i + 2, 2] = statisticsT.MaxValue;
            excelApplication.Cells[i + 2, 3] = statisticsP.MaxValue;
            excelApplication.Cells[i + 2, 4] = statisticsH.MaxValue;
            i++;

            excelApplication.Cells[i + 2, 1] = "Average";
            excelApplication.Cells[i + 2, 2] = statisticsT.Average;
            excelApplication.Cells[i + 2, 3] = statisticsP.Average;
            excelApplication.Cells[i + 2, 4] = statisticsH.Average;
            i++;


             excelWorkbook.Save();
             excelWorkbook.Close();
             excelApplication.Quit();

           
             //ReleaseComObject(excelWorksheet2);
             ReleaseComObject(excelWorksheet);
             ReleaseComObject(excelWorkbook);
             ReleaseComObject(excelApplication);

         }


        public static void CreateChart(string path, int numRegistos)
        {

            Excel.Application excelApplication = new Excel.Application();
            excelApplication.Visible = false;
            Excel.Workbook excelWorkbook = excelApplication.Workbooks.Open(path);
            Excel.Worksheet excelWorksheet = excelWorkbook.Worksheets.get_Item(1);

            //Add chart
            Excel.Chart myChart = null;
            Excel.ChartObjects charts = excelWorksheet.ChartObjects();
            Excel.ChartObject chartObject = charts.Add(10, 10, 300, 300); //left, top, width, heigh
            myChart = chartObject.Chart;

            string lastPosition = "D" + (numRegistos+1);

            //set chart range
            Excel.Range myrange = excelWorksheet.get_Range("A1", lastPosition); 
            myChart.SetSourceData(myrange);

            //chart properties using the named parameters and default parameters functionality in the .NET Framework 4.0
            myChart.ChartType = Excel.XlChartType.xlLine;
            myChart.ChartWizard(Source: myrange,
            Title: "Data Sensors Grafic", //valor do X
            CategoryTitle: "Date interval",
            ValueTitle: "Temperature"); //valor do Y

            myChart.DisplayBlanksAs = Excel.XlDisplayBlanksAs.xlInterpolated;
            myChart.Axes(Excel.XlCategoryType.xlCategoryScale).CategoryType = Excel.XlCategoryType.xlCategoryScale;
    //ActiveChart.Axes(xlCategory).CategoryType = xlCategoryScale

            excelWorkbook.Save();
            excelWorkbook.Close();
            excelApplication.Quit();

            ReleaseComObject(myrange);
            ReleaseComObject(charts);
            ReleaseComObject(excelWorksheet);
            ReleaseComObject(chartObject);
            ReleaseComObject(myChart);
            ReleaseComObject(excelWorkbook);
            ReleaseComObject(excelApplication);
        }

        //FUNÇÃO QUE LIBERTA A MEMÓRIA DOS OBJECTOS
        private static void ReleaseComObject(Object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                System.Diagnostics.Debug.WriteLine("Exception occured while releasing object" + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
