﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace DBPersistence
{
    class MyXmlHandler
    {
        private bool _isvalid;
        private string _validateMessage;

        public string ValidateMessage
        {
            get { return _validateMessage; }
        }

        private string _xmlfilepath;
        private string _xsdfilepath;

        public MyXmlHandler(String xmlFile, String xsdFile)
        {
            _xmlfilepath = xmlFile;
            _xsdfilepath = xsdFile;
        }

        public bool ValidateXml()
        {
            _isvalid = true;
            _validateMessage = "";

            XmlDocument doc = new XmlDocument();

            try
            {
                doc.Load(_xmlfilepath);
                ValidationEventHandler eventHandler = new ValidationEventHandler(MyHandler);
                doc.Schemas.Add(null, _xsdfilepath);
                doc.Validate(eventHandler);
            }
            catch (Exception ex)
            {
                _isvalid = false;
                _validateMessage = string.Format("ERROR: {0}", ex.ToString());
            }

            return _isvalid;
        }

        private void MyHandler(object sender, ValidationEventArgs e)
        {
            _isvalid = false;

            switch (e.Severity) // GRAVIDADE DO ERRO
            {
                case XmlSeverityType.Error:
                    _validateMessage += string.Format("ERROR: {0}", e.Message);
                    break;
                case XmlSeverityType.Warning:
                    _validateMessage += string.Format("WARNING: {0}", e.Message);
                    break;
                default:
                    break;
            }
        }


    }
}
