﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZeroMQ;
using SensorDataController;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using System.Xml;
using System.IO;

namespace DBPersistence
{

    class Program
    {
        static private XmlDocument doc;
        static private int currentNumberOfOccurrences;
        static private String alarmStatisticsXml = DBPersistence.Properties.Settings.Default.xmlAlarmeStatistics;

        static private string xmlName;
        static private string xsdName;

        static void Main(string[] args)
        {
            xmlName = "AlarmingStatistics.xml";
            xsdName = "AlarmingStatistics.xsd";

            Thread threadTemperatura = new Thread(ThreadTemperatura);
            Thread threadPressao = new Thread(ThreadPressao);
            Thread threadHumidade = new Thread(ThreadHumidade);



            currentNumberOfOccurrences = -1;     //tem que ser inicializado a 1 por causa do Int32.TryParse

            #region Abertura do ficheiro XML das Estatísticas
            doc = new XmlDocument();

            //Verifica se o ficheiro já existe (abre-o, e tenta localizar o primeiro elemento base)
            if (!File.Exists(alarmStatisticsXml))
            {
                //criar ficheiro de raiz
                XmlDeclaration declaracao = doc.CreateXmlDeclaration("1.0", null, null);
                doc.AppendChild(declaracao);

                XmlElement root = doc.CreateElement("alarmStatistics");
                doc.AppendChild(root);

                doc.Save(alarmStatisticsXml);

                StartThreads(threadTemperatura, threadPressao, threadHumidade);
            }
            else
            {
                //validar xml com xsd
                Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory + xmlName, AppDomain.CurrentDomain.BaseDirectory + xsdName);
                MyXmlHandler myHandler = new MyXmlHandler(AppDomain.CurrentDomain.BaseDirectory + xmlName, AppDomain.CurrentDomain.BaseDirectory + xsdName);
                Console.WriteLine(myHandler.ValidateXml().ToString() + " --> " + myHandler.ValidateMessage);

                if (myHandler.ValidateXml().ToString().Equals("True"))
                {
                    doc.Load(alarmStatisticsXml);
                    StartThreads(threadTemperatura, threadPressao, threadHumidade);
                }
                else
                {
                    Console.WriteLine("XML INVÁLIDO");
                    Console.ReadKey();
                }



            }
            #endregion


            //FAZER 3 THREADS COM 3 METODOS DIFERENTES QUE VAI TER CADA UM UM CICLO






            Console.WriteLine("Threads: Worker threads has terminated.");

        }

        private static void StartThreads(Thread threadTemperatura, Thread threadPressao, Thread threadHumidade)
        {
            threadTemperatura.Start();
            threadPressao.Start();
            threadHumidade.Start();


            while (!threadTemperatura.IsAlive) ;
            while (!threadPressao.IsAlive) ;
            while (!threadHumidade.IsAlive) ;

            Thread.Sleep(1);

            threadTemperatura.Join();
            threadPressao.Join();
            threadHumidade.Join();
        }

        //REGIAO DAS THREADS
        // This method will be called when the thread is started. 

        public static void ThreadTemperatura()
        {
            string connect_to_Temperatura = "tcp://" + DBPersistence.Properties.Settings.Default.pubTemperatura;
            ThreadsWorking(connect_to_Temperatura, "Temperatura");
        }

        public static void ThreadPressao()
        {
            string connect_to_Pressao = "tcp://" + DBPersistence.Properties.Settings.Default.pubPressao;
            ThreadsWorking(connect_to_Pressao, "Pressao");
        }

        public static void ThreadHumidade()
        {
            string connect_to_Humidade = "tcp://" + DBPersistence.Properties.Settings.Default.pubHumidade;
            ThreadsWorking(connect_to_Humidade, "Humidade");
        }

        public static void ThreadsWorking(String connection, String typeSensor)
        {
            var context = new ZContext();
            var subscriber = new ZSocket(context, ZSocketType.SUB);

            SqlConnection conn = null;
            conn = new SqlConnection(DBPersistence.Properties.Settings.Default.slqConnectionString);
            subscriber.Connect(connection);

            Console.WriteLine("Thread " + typeSensor + ": Connecting to {0}...", connection);
            Console.WriteLine("Thread " + typeSensor + ": is working..." + Environment.NewLine);

            while (true)
            {
                subscriber.SubscribeAll();
                var replyFrame = subscriber.ReceiveFrame();

                string reply = replyFrame.ReadString();
                SensorDados sensorDados = JsonConvert.DeserializeObject<SensorDados>(reply);
                Console.WriteLine(sensorDados);

                //ESTATISTICAS
                statisticsInsert(sensorDados);
                dbInsert(conn, sensorDados);
            }
        }

        private static void statisticsInsert(SensorDados sensorDados)
        {
            String date = sensorDados.dataTime.ToString("yyyy-MM-dd");

            XmlElement dailyStatistics = (XmlElement)doc.SelectSingleNode("/alarmStatistics/dailyStatistics[@date='" + date + "']");
            if (dailyStatistics == null)
            {
                dailyStatistics = doc.CreateElement("dailyStatistics");
                dailyStatistics.SetAttribute("date", date);
                XmlNode root = doc.SelectSingleNode("/alarmStatistics");
                root.AppendChild(dailyStatistics);
            }

            XmlElement sensor = (XmlElement)doc.SelectSingleNode("/alarmStatistics/dailyStatistics[@date='" + date + "']/sensor[@category='" + sensorDados.tipo + "']");
            if (sensor == null)
            {
                sensor = doc.CreateElement("sensor");
                sensor.SetAttribute("category", sensorDados.tipo);
                sensor.SetAttribute("numberOfOccurrences", "1");
                dailyStatistics.AppendChild(sensor);

                sensor.AppendChild(doc.CreateElement("min"));
                sensor.AppendChild(doc.CreateElement("max"));
                sensor.AppendChild(doc.CreateElement("avg"));
            }
            else
            {
                Int32.TryParse(sensor.GetAttribute("numberOfOccurrences"), out currentNumberOfOccurrences);
                currentNumberOfOccurrences = currentNumberOfOccurrences + 1;
                Console.WriteLine("currentNumberOfOccurrences = " + currentNumberOfOccurrences);
                sensor.SetAttribute("numberOfOccurrences", currentNumberOfOccurrences.ToString());
            }

            //Verifica se é menor que o atual min
            XmlNode statisticsMinValue = sensor.SelectSingleNode("min");
            if (statisticsMinValue.InnerText == "")
                statisticsMinValue.InnerText = sensorDados.valor.ToString().Replace(',', '.');
            else
            {
                double currentMin = Convert.ToDouble(statisticsMinValue.InnerText.Replace('.', ','));
                if (sensorDados.valor < currentMin)
                    statisticsMinValue.InnerText = sensorDados.valor.ToString().Replace(',', '.');
            }


            //Verifica se é maior que o atual max
            XmlNode statisticMaxValue = sensor.SelectSingleNode("max");
            if (statisticMaxValue.InnerText == "")
                statisticMaxValue.InnerText = sensorDados.valor.ToString().Replace(',', '.');
            else
            {
                double currentMax = Convert.ToDouble(statisticMaxValue.InnerText.Replace('.', ','));
                if (sensorDados.valor > currentMax)
                    statisticMaxValue.InnerText = sensorDados.valor.ToString().Replace(',', '.');
            }


            //Atualiza a média
            XmlNode statisticAvgValue = sensor.SelectSingleNode("avg");
            if (statisticAvgValue.InnerText == "" || currentNumberOfOccurrences == 1)
                statisticAvgValue.InnerText = sensorDados.valor.ToString().Replace(',', '.');
            else
            {
                double currentAvg = Convert.ToDouble(statisticAvgValue.InnerText.Replace('.', ','));
                currentAvg = (currentAvg * (currentNumberOfOccurrences - 1) + sensorDados.valor) / (currentNumberOfOccurrences);
                statisticAvgValue.InnerText = currentAvg.ToString().Replace(',', '.');
            }
            doc.Save(alarmStatisticsXml);
            Console.WriteLine("File updated!");
        }

        private static void dbInsert(SqlConnection conn, SensorDados sensorDados)
        {
            try
            {
                conn.Open();
                //ESCREVER NA BD
                SqlCommand cmd = new SqlCommand("INSERT INTO DadosSensor (IdSensor,TipoSensor,Valor,DataHora) VALUES ( @idSensor, @tipoSensor, @valor, @dataHora)", conn);
                cmd.Parameters.Add("@idSensor", SqlDbType.Int);
                cmd.Parameters["@idSensor"].Value = sensorDados.idSensor;

                cmd.Parameters.Add("@tipoSensor", SqlDbType.Char);
                cmd.Parameters["@tipoSensor"].Value = sensorDados.tipo;

                cmd.Parameters.Add("@valor", SqlDbType.Decimal);
                cmd.Parameters["@valor"].Value = sensorDados.valor;

                cmd.Parameters.Add("@dataHora", SqlDbType.DateTime);
                cmd.Parameters["@dataHora"].Value = sensorDados.dataTime;


                int resultadoQuery = cmd.ExecuteNonQuery();

                if (resultadoQuery > 0)
                    Console.WriteLine("Inseriu com sucesso");
                else
                    Console.WriteLine("Não Inseriu");

                conn.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }


}

