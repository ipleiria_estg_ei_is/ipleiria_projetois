﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SensorDataController
{
    [Serializable]
    public class SensorDados
    {
        public string tipo { get; set; }
        public double valor { get; set; }
        public int idSensor { get; set; }
        public DateTime dataTime { get; set; }

        public SensorDados(int idSensor, string tipo, double valor)
        {
            this.tipo = tipo;
            this.valor = valor;
            this.idSensor = idSensor;
            this.dataTime = DateTime.Now;
        }

        public override string ToString()
        {
            return "IdSensor: " + this.idSensor + Environment.NewLine + "Tipo: " + this.tipo + Environment.NewLine + "Valor: " + this.valor + Environment.NewLine;
        }
    }
}
