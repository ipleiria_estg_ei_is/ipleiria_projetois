﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SensorNodeDll;
using System.Globalization;
using ZeroMQ;
using SensorDataController;
using System.Web.Script.Serialization;

namespace SensorDataController
{
    class Program
    {
        public static ZContext context;
        public static ZSocket publisherHumidade;
        public static ZSocket publisherPressao;
        public static ZSocket publisherTemperatura;

        static void Main(string[] args)
        {
            SensorNodeDll.SensorNodeDll dll = new SensorNodeDll.SensorNodeDll();

            dll.Initialize(getData, SensorDataController.Properties.Settings.Default.timeDelayDll);  //inicializa a dll já com os dados

            
            context = new ZContext();
            publisherHumidade = new ZSocket(context, ZSocketType.PUB);
            publisherPressao = new ZSocket(context, ZSocketType.PUB);
            publisherTemperatura = new ZSocket(context, ZSocketType.PUB);

            //Canal de Humidade
            string adressHumidade = "tcp://*:" + SensorDataController.Properties.Settings.Default.portoHumidade; // MANDA PARA TODOS QUE ESTÃO A ESCUTA NAQUELE PORTO, ISTO PORQUE SE QUISERMOS ACRESTAAR MAIS ALGUM MODULO ELE JÁ ESTÁ PREPARADO
            publisherHumidade.Bind(adressHumidade);

            //Canal de Temperatura
            string adressTemperatura = "tcp://*:" + SensorDataController.Properties.Settings.Default.portoTemperatura; // MANDA PARA TODOS QUE ESTÃO A ESCUTA NAQUELE PORTO, ISTO PORQUE SE QUISERMOS ACRESTAAR MAIS ALGUM MODULO ELE JÁ ESTÁ PREPARADO
            publisherTemperatura.Bind(adressTemperatura);

            //Canal de Pressao
            string adressPressao = "tcp://*:" + SensorDataController.Properties.Settings.Default.portoPressao; // MANDA PARA TODOS QUE ESTÃO A ESCUTA NAQUELE PORTO, ISTO PORQUE SE QUISERMOS ACRESTAAR MAIS ALGUM MODULO ELE JÁ ESTÁ PREPARADO
            publisherPressao.Bind(adressPressao);

        }

        public static void getData(string dados)
        {
            Console.WriteLine(dados);

            string[] dadosSeparados = dados.Split(';');

            SensorDados sensorDados = new SensorDados(Int32.Parse(dadosSeparados[0]), dadosSeparados[1], Double.Parse(dadosSeparados[2], CultureInfo.InvariantCulture));
            //Converter a class para json
            var json = new JavaScriptSerializer().Serialize(sensorDados);
            var updateFrame = new ZFrame(json);

            switch (dadosSeparados[1])
            {
                case "T":
                    publisherTemperatura.Send(updateFrame);
                    break;

                case "H":
                    publisherHumidade.Send(updateFrame);
                    break;

                case "P":
                    publisherPressao.Send(updateFrame);
                    break;
            }
        }

    }
}
