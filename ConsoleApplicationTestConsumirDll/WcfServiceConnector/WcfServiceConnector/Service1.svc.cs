﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceConnector
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        private static string slqConnectionString = WcfServiceConnector.Properties.Settings.Default.slqConnectionString;
        private static SqlConnection con;
        
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public SensorDados[] GetIntervalDate(char typeSensor, string startDate, string endDate)
        {
            try
            {
                con = new SqlConnection(slqConnectionString);

                List<SensorDados> sensorsDados = new List<SensorDados>();

                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT IdSensor,TipoSensor,Valor,DataHora FROM DadosSensor WHERE (DataHora BETWEEN @startDate AND @endDate) AND TipoSensor = @typeSensor;", con);

                cmd.Parameters.AddWithValue("@typeSensor", typeSensor);
                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);


                SqlDataReader readerBD = cmd.ExecuteReader();
                while (readerBD.Read())
                {
                    sensorsDados.Add(new SensorDados(readerBD.GetInt32(0), readerBD.GetString(1).Trim(), readerBD.GetDecimal(2), readerBD.GetDateTime(3)));
                }

                readerBD.Close();
                con.Close();
                return sensorsDados.ToArray();
            }
            catch (Exception)
            { 
                throw;
            }
        }

        public SensorDados[] GetIntervalDate2Sensors(char typeSensor, char typeSensor2, string startDate, string endDate)
        {
            try
            {
                con = new SqlConnection(slqConnectionString);

                List<SensorDados> sensorsDados = new List<SensorDados>();

                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT IdSensor,TipoSensor,Valor,DataHora FROM DadosSensor WHERE (DataHora BETWEEN @startDate AND @endDate) AND (TipoSensor = @typeSensor OR TipoSensor = @typeSensor2);", con);

                cmd.Parameters.AddWithValue("@typeSensor", typeSensor);
                cmd.Parameters.AddWithValue("@typeSensor2", typeSensor2);
                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);


                SqlDataReader readerBD = cmd.ExecuteReader();
                while (readerBD.Read())
                {
                    sensorsDados.Add(new SensorDados(readerBD.GetInt32(0), readerBD.GetString(1).Trim(), readerBD.GetDecimal(2), readerBD.GetDateTime(3)));
                }

                readerBD.Close();
                con.Close();
                return sensorsDados.ToArray();
            }
            catch (Exception)
            { 
                throw;
            }
        }


        public SensorDados[] GetIntervalDate3Sensors(char typeSensor, char typeSensor2, char typeSensor3, string startDate, string endDate)
        {
            try
            {
                con = new SqlConnection(slqConnectionString);

                List<SensorDados> sensorsDados = new List<SensorDados>();

                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT IdSensor,TipoSensor,Valor,DataHora FROM DadosSensor WHERE (DataHora BETWEEN @startDate AND @endDate) AND (TipoSensor = @typeSensor OR TipoSensor = @typeSensor2 OR TipoSensor = @typeSensor3);", con);

                cmd.Parameters.AddWithValue("@typeSensor", typeSensor);
                cmd.Parameters.AddWithValue("@typeSensor2", typeSensor2);
                cmd.Parameters.AddWithValue("@typeSensor3", typeSensor3);
                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);


                SqlDataReader readerBD = cmd.ExecuteReader();
                while (readerBD.Read())
                {
                    sensorsDados.Add(new SensorDados(readerBD.GetInt32(0), readerBD.GetString(1).Trim(), readerBD.GetDecimal(2), readerBD.GetDateTime(3)));
                }

                readerBD.Close();
                con.Close();
                return sensorsDados.ToArray();
            }
            catch (Exception)
            { 
                throw;
            }
        }
        


        public StatisticValues GetStatisticIntervalDate(char typeSensor, string startDate, string endDate)
        {
            try
            {
                con = new SqlConnection(slqConnectionString);

                StatisticValues statisticValues = null;

                con.Open();

                SqlCommand cmd = new SqlCommand("SELECT AVG(Valor), MIN(Valor), MAX(Valor) FROM DadosSensor WHERE (DataHora BETWEEN @startDate AND @endDate) AND TipoSensor = @typeSensor;", con);

                cmd.Parameters.AddWithValue("@typeSensor", typeSensor);
                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                SqlDataReader readerBD = cmd.ExecuteReader();

                while (readerBD.Read())
                {
                    if (!readerBD.IsDBNull(0))
                    {
                        statisticValues = new StatisticValues(readerBD.GetDecimal(0), readerBD.GetDecimal(1), readerBD.GetDecimal(2));
                    }

                }
               

                con.Close();
                return statisticValues;
            }
            catch (Exception)
            {
                throw;
            }
        }

     }
}
