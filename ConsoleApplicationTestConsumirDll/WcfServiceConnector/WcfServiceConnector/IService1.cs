﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceConnector
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        SensorDados[] GetIntervalDate(char typeSensor, string startDate, string endDate);

        [OperationContract]
        SensorDados[] GetIntervalDate2Sensors(char typeSensor, char typeSensor2, string startDate, string endDate);

        [OperationContract]
        SensorDados[] GetIntervalDate3Sensors(char typeSensor, char typeSensor2, char typeSensor3, string startDate, string endDate);


        [OperationContract]
        StatisticValues GetStatisticIntervalDate(char typeSensor, string startDate, string endDate);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here
    }

    [DataContract]
    public class SensorDados
    {

        private int idSensor;
        private string tipoSensor;
        private decimal valor;
        private DateTime data;


        public SensorDados(int idSensor, string tipoSensor, decimal valor, DateTime data)
        {
            this.idSensor = idSensor;
            this.tipoSensor = tipoSensor;
            this.valor = valor;
            this.data = data;
        }

        [DataMember]
        public int IdSensor
        {
            get { return idSensor; }
            set { idSensor = value; }
        }

        [DataMember]
        public string TipoSensor
        {
            get { return tipoSensor; }
            set { tipoSensor = value; }
        }

        [DataMember]
        public decimal Valor
        {
            get { return valor; }
            set { valor = value; }
        }

        [DataMember]
        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        public override string ToString()
        {
            return "IdSensor: " + this.IdSensor + Environment.NewLine + "Tipo: " + this.TipoSensor + Environment.NewLine + "Valor: " + this.Valor + Environment.NewLine;
        }
    }

    [DataContract]
    public class StatisticValues
    {

        private decimal average;
        private decimal minValue;
        private decimal maxValue;

        public StatisticValues(decimal average, decimal minValue, decimal maxValue)
        {
            this.average = average;
            this.minValue = minValue;
            this.maxValue = maxValue;

        }

        [DataMember]
        public decimal Average
        {
            get { return average; }
            set { average = value; }
        }

        [DataMember]
        public decimal MinValue
        {
            get { return minValue; }
            set { minValue = value; }
        }

        [DataMember]
        public decimal MaxValue
        {
            get { return maxValue; }
            set { maxValue = value; }
        }

    }
    



    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
