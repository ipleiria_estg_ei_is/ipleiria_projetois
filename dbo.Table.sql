﻿CREATE TABLE [dbo].[DadosSensor]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [IdSensor] INT NULL, 
    [TipoSensor] CHAR NULL, 
    [Valor] DECIMAL(8, 2) NULL, 
    [DataHora] DATETIME NULL
)
